FROM cypress/base:10
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
CMD [ "node_modules/.bin/cypress", "open" ]  
CMD [ "node_modules/.bin/cypress", "run", "--headed" ]  